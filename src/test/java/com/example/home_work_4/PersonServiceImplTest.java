package com.example.home_work_4;

import com.example.home_work_4.modal.Person;
import com.example.home_work_4.service.PersonServiceImpl;
import com.example.home_work_4.utils.JdbcConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class PersonServiceImplTest {
    @Test
    void createPerson() {
        Person expectedPerson = new PersonServiceImpl().createPerson(new Person(99, "CreatePersonTest", 12, "testing@tes.com"));
        Person actualPerson = null;
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("SELECT * FROM Person WHERE id=?");
            preparedStatement.setInt(1, 99);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            actualPerson = new Person();
            actualPerson.setId(resultSet.getInt("id"));
            actualPerson.setName(resultSet.getString("name"));
            actualPerson.setEmail(resultSet.getString("email"));
            actualPerson.setAge(resultSet.getInt("age"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");

        new PersonServiceImpl().deletePerson(99);
    }

    @Test
    void updatePerson() {
        new PersonServiceImpl().createPerson(new Person(99, "CreatePersonTest", 12, "testing@tes.com"));
        Person expectedPerson = new PersonServiceImpl().updatePerson(new Person(99, "UpdatePersonTest", 22, "testing@tes.com"));
        Person actualPerson = null;
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("SELECT * FROM Person WHERE id=?");
            preparedStatement.setInt(1, 99);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            actualPerson = new Person();
            actualPerson.setId(resultSet.getInt("id"));
            actualPerson.setName(resultSet.getString("name"));
            actualPerson.setEmail(resultSet.getString("email"));
            actualPerson.setAge(resultSet.getInt("age"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");

        new PersonServiceImpl().deletePerson(99);
    }

    @Test
    void getPerson() {
        Person expectedPerson = new PersonServiceImpl().createPerson(new Person(99, "CreatePersonTest", 12, "testing@tes.com"));
        Person actualPerson = new PersonServiceImpl().getPerson(99);

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");

        new PersonServiceImpl().deletePerson(99);
    }

    @Test
    void getAllPerson() {
        List<Person> expectedPerson = new ArrayList<>();

        try {
            Statement statement = JdbcConnection.getConnection().createStatement();
            String SQL = "SELECT * FROM Person";
            ResultSet resultSet = statement.executeQuery(SQL);

            while (resultSet.next()) {
                Person person = new Person();

                person.setId(resultSet.getInt("id"));
                person.setName(resultSet.getString("name"));
                person.setEmail(resultSet.getString("email"));
                person.setAge(resultSet.getInt("age"));

                expectedPerson.add(person);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Person> actualPerson = new PersonServiceImpl().getAllPerson();

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");
    }

    @Test
    void deletePerson() {
        new PersonServiceImpl().createPerson(new Person(99, "CreatePersonTest", 12, "testing@tes.com"));
        Person createdPerson = new PersonServiceImpl().getPerson(99);
        Assertions.assertNotNull(createdPerson);

        new PersonServiceImpl().deletePerson(99);
        Person actualPerson = new PersonServiceImpl().getPerson(99);
        Person expectedPerson = new Person();
        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");
    }
}