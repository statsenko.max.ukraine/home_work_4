package com.example.home_work_4;

import com.example.home_work_4.modal.Address;
import com.example.home_work_4.service.AddressServiceImpl;
import com.example.home_work_4.utils.JdbcConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SpringBootTest
public class AddressServiceImplTest {
    @Test
    void createAddress() {
        Address expectedAddress = new AddressServiceImpl().createAddress(new Address(99, "London"));
        Address actualAddress = null;
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("SELECT * FROM Address WHERE id=?");
            preparedStatement.setInt(1, 99);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            actualAddress = new Address();
            actualAddress.setId(resultSet.getInt("id"));
            actualAddress.setAddress(resultSet.getString("address"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(expectedAddress, actualAddress, "Address objects don't match");

        new AddressServiceImpl().deleteAddress(99);
    }

    @Test
    void updateAddress() {
        new AddressServiceImpl().createAddress(new Address(99, "London"));
        Address expectedAddress = new AddressServiceImpl().updateAddress(new Address(99, "Berlin"));
        Address actualAddress = null;
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("SELECT * FROM Address WHERE id=?");
            preparedStatement.setInt(1, 99);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            actualAddress = new Address();
            actualAddress.setId(resultSet.getInt("id"));
            actualAddress.setAddress(resultSet.getString("address"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals(expectedAddress, actualAddress, "Address objects don't match");

        new AddressServiceImpl().deleteAddress(99);
    }

    @Test
    void getAddress() {
        Address expectedAddress = new AddressServiceImpl().createAddress(new Address(99, "London"));
        Address actualAddress = new AddressServiceImpl().getAddress(99);

        Assertions.assertEquals(expectedAddress, actualAddress, "Address objects don't match");

        new AddressServiceImpl().deleteAddress(99);
    }

    @Test
    void deleteAddress() {
        new AddressServiceImpl().createAddress(new Address(99, "London"));
        Address createdAddress = new AddressServiceImpl().getAddress(99);
        Assertions.assertNotNull(createdAddress);

        new AddressServiceImpl().deleteAddress(99);
        Address actualAddress = new AddressServiceImpl().getAddress(99);
        Address expectedAddress = new Address();
        Assertions.assertEquals(expectedAddress, actualAddress, "Address objects don't match");
    }
}
