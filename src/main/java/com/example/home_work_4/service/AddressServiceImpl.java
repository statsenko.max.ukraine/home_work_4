package com.example.home_work_4.service;

import com.example.home_work_4.modal.Address;
import com.example.home_work_4.utils.JdbcConnection;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class AddressServiceImpl implements AddressService {
    @Override
    public Address createAddress(Address address) {
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("INSERT INTO Address VALUES(?, ?)");
            preparedStatement.setInt(1, address.getId());
            preparedStatement.setString(2, address.getAddress());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public Address updateAddress(Address address) {
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("UPDATE Address SET address=? WHERE id=?");
            preparedStatement.setString(1, address.getAddress());
            preparedStatement.setInt(2, address.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public Address getAddress(Integer id) {
        Address address = null;
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("SELECT * FROM Address WHERE id=?");
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();

            address = new Address();
            address.setId(resultSet.getInt("id"));
            address.setAddress(resultSet.getString("address"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return address;
    }

    @Override
    public void deleteAddress(Integer id) {
        try {
            PreparedStatement preparedStatement = JdbcConnection.getConnection().prepareStatement("DELETE FROM Address WHERE id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
