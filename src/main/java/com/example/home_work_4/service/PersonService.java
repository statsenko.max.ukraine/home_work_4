package com.example.home_work_4.service;

import com.example.home_work_4.modal.Person;

import java.util.List;

public interface PersonService {

    Person createPerson(Person person);

    Person updatePerson(Person person);

    Person getPerson(Integer id);

    List<Person> getAllPerson();

    void deletePerson(Integer id);
}
