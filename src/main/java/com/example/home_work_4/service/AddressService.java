package com.example.home_work_4.service;

import com.example.home_work_4.modal.Address;

public interface AddressService {

    Address createAddress(Address address);

    Address updateAddress(Address address);

    Address getAddress(Integer id);

    void deleteAddress(Integer id);
}
