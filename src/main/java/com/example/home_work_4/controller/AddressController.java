package com.example.home_work_4.controller;

import com.example.home_work_4.modal.Address;
import com.example.home_work_4.service.AddressService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @PostMapping("/createAddress")
    public Address createPerson(@RequestBody Address address) {
        return addressService.createAddress(address);
    }

    @GetMapping("/getAddress{id}")
    public Address getAddress(@PathVariable("id") @NonNull Integer id) {
        return addressService.getAddress(id);
    }

    @PostMapping("/updateAddress")
    public Address updateAddress(@RequestBody Address address) {
        return addressService.updateAddress(address);
    }

    @DeleteMapping("/deleteAddress{id}")
    public void deleteAddress(@PathVariable("id") @NonNull Integer id) {
        addressService.deleteAddress(id);
    }
}
