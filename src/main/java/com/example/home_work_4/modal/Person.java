package com.example.home_work_4.modal;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Person {
    private Integer id;
    private String name;
    private Integer age;
    private String email;
}
